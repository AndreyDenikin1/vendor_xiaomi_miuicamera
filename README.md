# Leica MiuiCamera

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/helliscloser/vendor_xiaomi_miuicamera -b leica vendor/xiaomi/miuicamera
```

Make these changes in **sm6150-common**

**sm6150.mk**
```
# MiuiCamera
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```

## Credits

### https://t.me/itzdfplayer_stash For Leica Camera Mod for davinci and fixing most issues. <br>
